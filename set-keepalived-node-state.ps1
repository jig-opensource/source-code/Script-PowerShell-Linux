#!/usr/bin/pwsh

# Macht den Host zur aktiven oder inaktiven keepalived Node

# 001, 240529, tom@jig.ch

# !Ex
# Macht den Host zur aktiven keepalived node
# 	/usr/local/bin/set-keepalived-node-state.ps1 -active
# Macht den Host zur inaktiven keepalived node
# 	/usr/local/bin/set-keepalived-node-state.ps1 -inactive


[CmdletBinding()]
param(
  [Switch]$Active,
  [Switch]$Inactive
)


## Prepare



## Config
$KeepalivedCfg = '/etc/keepalived/keepalived.conf'
$Col1 = 16



## Functions


# Liefert die IP-Adressen des Hosts
Function Get-Linux-Host-IPAddrs() {
  # Die IP-Adressen des Hosts abrufen
  $ThisHostIPS = $(ip -o -4 addr show scope global | awk '{print $4}' | cut -d '/' -f1)
  $ThisHostIPS 
}


# Abrufen, welche IP-Adresse Keepalived als Shared Adresse nützt
Function Get-Keepalived-Virtual-IPAddrs($KeepalivedCfg) {
  $VirtualIPAddr = Get-Content $KeepalivedCfg -Raw | `
                   Select-String -Pattern '.*\bvirtual_ipaddress\b\s*{\s*(.*)\s*}' | % {
                     $_.Matches.Groups[1].Value.Trim()
                   }
  $VirtualIPAddr
}


# Liest die Keepalived Service …State Properties
Function Get-Keepalived-Service-States() {
  # Die Service Status Props abrufen

  # Varianten:
  # systemctl show -p ActiveState --value keepalived
  # systemctl show -p SubState --value keepalived
  # systemctl is-active --quiet keepalived && echo Service is running
  # systemctl show keepalived | grep --color=never State=

  # Infos zum keepalived service abrufen
  $Res = systemctl show keepalived
  # Hashtable erzeugen
  $ResStates = $Res | ConvertFrom-StringData
  $htStates = @{}
  $ResStates | % {
    # $ItemEnum = $_.GetEnumerator()
    # Alle *State Properties in die HT übertragen
    $Item = $_.GetEnumerator() | Select Name, Value
    If ($Item.Name  -like "*State") {
      $htStates.Add( $Item.Name, $Item.Value )
    }
  }

  # Liefert diese Props:
  # $htStates.ActiveState   active
  # $htStates.SubState      running
  # $htStates.LoadState     loaded
  # $htStates.UnitFileState enabled
  # $htStates.FreezerState  running

  $htStates
}



## Prepare

# Daten bestimmen
$VirtualIPAddr = Get-Keepalived-Virtual-IPAddrs $KeepalivedCfg
$ThisHostIPS = Get-Linux-Host-IPAddrs

# Hat der Host die Keepalived Shared IP-Adresse?
$HostIsActiveNode = ($ThisHostIPS | ? { $_.Trim() -eq $VirtualIPAddr.Trim() }).Count -ge 1



## Main

# Ist die Keepalived-Node aktiv oder passiv?
Write-Host "`nAktueller Host-Status: " -ForegroundColor Yellow -NoNewLine
If ( $HostIsActiveNode ) {
  Write-Host 'Aktive Node' -ForegroundColor Magenta
} Else {
  Write-Host 'Inaktive Node' -ForegroundColor Magenta
}

# Params prüfen
If (-not $Active -and -not $Inactive) {
  Write-Host 'Ein Parameter muss angegeben werden: -active oder -inactive' -ForegroundColor Red
  Exit
}

# Den aktiven Host aktivieren?
If ($Active -and $HostIsActiveNode) {
  Write-Host 'Der Host ist schon die aktive Keepalived-Node' -ForegroundColor Green
  Exit
}

# Den inaktiven Host deaktivieren?
If ($Inactive -and $HostIsActiveNode -eq $False) {
  Write-Host 'Der Host ist schon die inaktive Keepalived-Node' -ForegroundColor Green
  Exit
}



# Den Host zur aktiven keepalived Node machen
If ($Active) {
  # Auf der aktiven Node muss keepalived gestoppt und wieder gestartet werden
  # Die aktive Node ist via Keepalived shared IP-Adresse erreichbar
  Write-Host 'Deaktiviere die andere Node'
  Invoke-Expression "ssh -o BatchMode=yes -o ServerAliveInterval=1 -t root@10.1.20.33 'sudo service keepalived stop && sleep 3s && sudo service keepalived start'" 2>&1 >$null

  # Den aktuellen State wieder erfassen
  $ThisHostIPS = Get-Linux-Host-IPAddrs
  # Hat der Host die Keepalived Shared IP-Adresse?
  $HostIsActiveNode = ($ThisHostIPS | ? { $_.Trim() -eq $VirtualIPAddr.Trim() }).Count -ge 1

  Write-Host "`nNeuer Host-Status: " -ForegroundColor Yellow -NoNewLine
  If ( $HostIsActiveNode ) {
    Write-Host 'Aktive Node' -ForegroundColor Magenta
    Write-Host ' > OK' -ForegroundColor Green
  } Else {
    Write-Host 'Inaktive Node' -ForegroundColor Magenta
    Write-Host ' > Wechsel hat nicht geklappt!' -ForegroundColor Red
  }

  Exit
}



# Den Host zur inaktiven keepalived Node machen
If ($Inactive) {
  # Den aktuellen Host zur inaktiven Node machen
  Write-Host 'Deaktiviere diesen Host als aktive Node'
  Invoke-Expression "sudo service keepalived stop && sleep 3s && sudo service keepalived start"

  # Den aktuellen State wieder erfassen
  $ThisHostIPS = Get-Linux-Host-IPAddrs
  # Hat der Host die Keepalived Shared IP-Adresse?
  $HostIsActiveNode = ($ThisHostIPS | ? { $_.Trim() -eq $VirtualIPAddr.Trim() }).Count -ge 1

  Write-Host "`nNeuer Host-Status: " -ForegroundColor Yellow -NoNewLine
  If ( $HostIsActiveNode ) {
    Write-Host 'Aktive Node' -ForegroundColor Magenta
    Write-Host ' > Wechsel hat nicht geklappt!' -ForegroundColor Red
  } Else {
    Write-Host 'Inaktive Node' -ForegroundColor Magenta
    Write-Host ' > OK' -ForegroundColor Green
  }

  Exit
}

