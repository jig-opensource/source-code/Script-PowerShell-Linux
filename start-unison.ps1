#!/usr/bin/pwsh

# Startet unison zum bidirektionalen Synchronisieren der nginx-config zwischen Master und Slave

# 001, 240527, tom@jig.ch
# 002, 240603
#   Erweiterung für Service nginx-ldap


[CmdletBinding()]
param(
  [Switch]$Active,
  [Switch]$Inactive
)


## Prepare


## Config
$UnisonProfileFiles = @(
    @{
        SyncPrf = 'sync-nginx.prf'
        ReloadSvcNginx = $True
        RestartSvcLdap = $False
    },
    @{
        SyncPrf = 'sync-nginx-ldap.prf'
        ReloadSvcNginx = $True
        RestartSvcLdap = $True
    },
    @{
        SyncPrf = 'sync-ssl-certs.prf'
        ReloadSvcNginx = $True
        RestartSvcLdap = $False
    },
    @{
        SyncPrf = 'sync-usr-local-bin.prf'
        ReloadSvcNginx = $False
        RestartSvcLdap = $False
    },
    @{
        SyncPrf = 'sync-var-www-html.prf'
        ReloadSvcNginx = $True
        RestartSvcLdap = $False
    }
)




## Funcs

# Liefert True, wenn StrArr mind. einen Text in StrArrSrchTexts beinhaltet
# $StrArr		          List Strings, in denen der Text gesucht wird
# $StrArrSrchTexts    Die Texte, die wir suchen
#                     e.g. $StrArrSrchTexts = @('---->', '====>')
Function Has-StrArr-StrArrSrchTexts($StrArr, $StrArrSrchTexts) {
  # Hat diei Unison Ausgabe Hinweis auf synchronisierte Files?
  $SrchPtrn = @('---->', '====>')

  $Found = $false
  ForEach ($SrchTxt in $StrArrSrchTexts) {
    If ($StrArr -like "*$SrchTxt*") {
      Return $True
    }
  }
  Return $False
}



## Init
# Hat unison Files synchronisiert?
$Restart_svc_nginx_Master = $False
$Restart_svc_nginx_Slave = $False

$Restart_svc_nginx_ldap_auth_Master = $False
$Restart_svc_nginx_ldap_auth_Slave = $False



## Start

# Alle Unison-Syncs starten und prüfen, ob Daten übertragen wurden
Write-Host ("`nStarte Sync:") -ForegroundColor Yellow
$UnisonProfileFiles | % {
  Write-Host ("> {0}" -f $_.SyncPrf) -ForegroundColor Cyan
  # Shell-Console komplett zu stdout schicken
  $ResUnison = $(unison -ui text $_.SyncPrf 2>&1)
  # $ResUnison

  # Hat die Unison Ausgabe Hinweis auf synchronisierte Files?
  # Files vom Master zum Slave synchronisiert?
  $Found = Has-StrArr-StrArrSrchTexts $ResUnison @('---->', '====>')
  If ($Found) {
    If ($_.ReloadSvcNginx) {
      $Restart_svc_nginx_Slave = $True
    }
    If ($_.RestartSvcLdap) {
      $Restart_svc_nginx_ldap_auth_Slave = $True
    }
    Write-Host "  > Master: Files wurden zum Slave synchronisiert" -ForegroundColor Magenta
  }

  # Files vom Slave zum Master synchronisiert?
  $Found = Has-StrArr-StrArrSrchTexts $ResUnison @('<----', '<====')
  If ($Found) {
    If ($_.ReloadSvcNginx) {
      $Restart_svc_nginx_Master = $True
    }
    If ($_.RestartSvcLdap) {
      $Restart_svc_nginx_ldap_auth_Master = $True
    }
    Write-Host "  > Slave: Files wurden zum Master synchronisiert" -ForegroundColor Magenta
  }

}


# Wenn Files synchronisiert wurden:
# nginx veranlassen, die Konfig neu einzulesen

If ($Restart_svc_nginx_Slave) {
  Write-Host "`nUnison hat Files zum Slave synchronisiert. Initiiere nginx reload." -ForegroundColor Yellow
  Write-Host '  > Lade nginx-config auf dem Slave' -ForegroundColor Green
  # ssh -t root@bapx006-2 'service nginx reload'
  $SshCommand = @"
    service nginx reload
"@

  # Shell-Console komplett zu stdout schicken
  # $Res = ssh -t root@bapx006-2 $SshCommand
  $Res = $(ssh -t root@bapx006-2 $SshCommand 2>&1)
  # Write-Host $Res
}


If ($Restart_svc_nginx_Master) {
  Write-Host "`nUnison hat Files zum Master synchronisiert. Initiiere nginx reload." -ForegroundColor Yellow
  Write-Host '  > Lade nginx-config auf dem Master' -ForegroundColor Green
  # Shell-Console komplett zu stdout schicken
  # service nginx reload
  $Res = $(service nginx reload 2>&1)
  # Write-Host $Res
}


# Den nginx-ldap Service veranlassen, neu zu starten und die Konfig neu einzulesen?
If ($Restart_svc_nginx_ldap_auth_Slave) {
  Write-Host "`nUnison hat Files zum Slave synchronisiert. Initiiere nginx-ldap Service restart" -ForegroundColor Yellow
  Write-Host '  > Starte nginx-ldap auf dem Slave neu' -ForegroundColor Green
  # ssh -t root@bapx006-2 'systemctl daemon-reload ; systemctl start nginx-ldap-auth.service'
  $SshCommand = @"
    systemctl daemon-reload ; systemctl start nginx-ldap-auth.service
"@

  # Shell-Console komplett zu stdout schicken
  # $Res = ssh -t root@bapx006-2 $SshCommand
  $Res = $(ssh -t root@bapx006-2 $SshCommand 2>&1)
  # Write-Host $Res
}

If ($Restart_svc_nginx_ldap_auth_Master) {
  Write-Host "`nUnison hat Files zum Master synchronisiert. Initiiere nginx-ldap Service restart" -ForegroundColor Yellow
  Write-Host '  > Starte nginx-ldap auf dem Master neu' -ForegroundColor Green
  # Shell-Console komplett zu stdout schicken
  # systemctl daemon-reload ; systemctl start nginx-ldap-auth.service
  $Res = $(systemctl daemon-reload 2>&1)
  # Write-Host $Res
  $Res = $(systemctl start nginx-ldap-auth.service 2>&1)
  # Write-Host $Res
}


