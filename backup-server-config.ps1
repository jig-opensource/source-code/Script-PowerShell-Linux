#!/usr/bin/pwsh

# Kopiert alle Config-Files dieses Servers in ein Backup-Dir

# !Ex 
# # Backup starten, default Backup-Dir nützen  ~/backup
# backup-server-config.ps1
# 
# # Backup starten, Backup-Dir: ~/backup240606
# backup-server-config.ps1 -BackupDir '~/backup240606'
# 


# 001, 240606, tom@jig.ch

[CmdletBinding()]
param(
  [Parameter(Position=0)]
  # Ziel-Verzeichnis fürs Backup, wenn nicht definiert wird ~/backup genützt
  # » Kriegt den Public Key
  [String]$BackupDir
)



## Allenfalls als root starten
If ( (id -u) -ne 0) {
  Write-Host 'Das Skript benötigt Root-Rechte. Starte es als root' -ForegroundColor Red
  # Konstruktion des Befehls für die Neuausführung mit sudo
  $command = ('sudo pwsh -File {0}' -f $MyInvocation.MyCommand.Path)
  # Ausführung des Befehls
  Invoke-Expression $command
  # Beenden des aktuellen Skripts, um Doppelungen zu vermeiden
  Exit
}



## Config
$DefaultBackupDir = '~/backup'
$Col1Width = 14


# Definition, welche Files kopiert werden müssen, um alle Config-Files des Servers zu sichern
$SrvcConfigBackupInfo = @(
  @{
    Name = 'Server: SSL Zertifikat (für nginx)'
    CopyProfiles = @(
      @{ Recursive = $False; Src = '/etc/ssl/'; PrintWarning = '❯ Die privaten SSH Zertifikate werden NICHT kopiert! ' }
    )
  }, @{
    Name = 'Server: Crontab'
    BackupJob = @{
      Name = 'Backup-Crontab'
      Param = 'root'
    }
 }, @{
    Name = 'Server: Netzwerk'
    CopyProfiles = @(
      @{ Recursive = $False; Src = '/etc/netplan/00-installer-config.yaml' }
    )
  }, @{
    Name = 'Service: SSH'
    CopyProfiles = @(
      @{ Recursive = $False; Src = '/etc/ssh/sshd_config' }
    )
  }, @{
    Name = 'Scripts'
    CopyProfiles = @(
      @{ Recursive = $True; Src = '/usr/local/bin/*.ps1' }
    )
  }, @{
    Name = 'Service: Keepalived'
    CopyProfiles = @(
      @{ Recursive = $True; Src = '/etc/keepalived/*' }
    )
  }, @{
    Name = 'Service: nginx'
    CopyProfiles = @(
      # Files
      @{ Recursive = $False; Src = '/etc/nginx/nginx.conf' }
      @{ Recursive = $False; Src = '/etc/nginx/nginx-ldap-auth-daemon' }
      @{ Recursive = $False; Src = '/etc/nginx/nginx-ldap.conf' }
      # Zur Sicherheit: alle *.conf Files
      # @{ Recursive = $True; Src = '/etc/nginx/*.conf' }
      # Dirs
      @{ Recursive = $True; Src = '/etc/nginx/html/*' }
      @{ Recursive = $True; Src = '/etc/nginx/sites-available/*' }
      @{ Recursive = $True; Src = '/etc/nginx/sites-enabled/*' }
    )
  }, @{
    Name = 'nginx: statische Files'
    CopyProfiles = @(
      @{ Recursive = $True; Src = '/var/www/html/*' }
    )
  }, @{
    Name = 'Nginx Addon: nginx-ldap-auth'
    CopyProfiles = @(
      @{ Recursive = $False; Src = '/etc/systemd/system/nginx-ldap-auth.service' }
      @{ Recursive = $False; Src = '/etc/nginx/nginx-ldap-auth-daemon' }
      @{ Recursive = $False; Src = '/etc/nginx/nginx-ldap.conf' }
    )
  }, @{
    Name = 'Tool: unison'
    CopyProfiles = @(
      @{ Recursive = $True; Src = '/root/.unison/*.sh' }
      @{ Recursive = $True; Src = '/root/.unison/*.ps1' }
      @{ Recursive = $True; Src = '/root/.unison/*.prf' }
    )
  }, @{
    Name = 'Git-Repos'
    CopyProfiles = @(
      @{ Recursive = $True; Src = '/usr/local/src/gitlab/*'; ExcludeSubDirs = @( '.git' ) }
    )
  }
)



## Functions


# Kopiert den Inhalt von crontab des Users $UserName in $Zieldatei
Function Backup-Crontab($UserName, $Zieldatei) {
  $Cmd = "su $UserName -c 'crontab -l'"
  $CrontabContent = Invoke-Expression $Cmd 2>$null
  If ( [String]::IsNullOrWhiteSpace($CrontabContent) ) {
		$Str = ('Crontab ist leer für den User {0}' -f $UserName)
    $Str | Out-File -FilePath $Zieldatei
  } Else {
    $CrontabContent | Out-File -FilePath $Zieldatei
  }
}



# Konvertiert einen String wie 'yellow' in [ConsoleColor]
# Bei einem Fehler wird White zurückgegeben
Function ConvertTo-ConsoleColor {
    Param (
        [String]$colorString
    )

    try {
        # Try to convert the string to a ConsoleColor
        $color = [ConsoleColor]::Parse([ConsoleColor], $colorString)
    } catch {
        Write-Error ("Unbekannte ConsoleColor: '{0}'" -f $colorString) -ForegroundColor Red
        # If there is an error, default to ConsoleColor.White
        $color = [ConsoleColor]::White
    }

    return $color
}


# Gibt Text in zwei Spalten aus
Function Write-Host-2Col() {
  Param (
    [Parameter()]
    [Int]$Ident=0,
    [Parameter(Mandatory, Position=0)]
    [Int]$Col1Width,
    [AllowEmptyString()]
    [Parameter(Mandatory, Position=1)]
    [String]$TxtCol1,
    [Parameter(Position=2)]
    [String]$TxtCol2,
    [Parameter(Position=3)]
    [String]$Col2FGColor = 'White'
  )

  $oFGColor = ConvertTo-ConsoleColor $Col2FGColor

  Write-Host ("{0}{1,-$Col1Width}: " -f ('  ' * $Ident), $TxtCol1) -NoNewLine
  Write-Host ("{0}" -f $TxtCol2) -ForegroundColor $oFGColor

}


# Kopiert alle Files aber schliesst Verzeichnisse aus
Function Copy-Item-ExcludeDirs {
  Param (
    [String]$SrcPath,
    [String]$DstPath,
    [String]$Pattern,
    [String[]]$ExcludeDirs,
    [Switch]$Recursive
  )

  # Write-Host ('$SrcPath: {0}' -f $SrcPath) -ForegroundColor Magenta
  # Write-Host ('$DstPath: {0}' -f $DstPath) -ForegroundColor Magenta
  # Write-Host ('$Pattern: {0}' -f $Pattern) -ForegroundColor Magenta
  # Write-Host ('$ExcludeDirs: {0}' -f $ExcludeDirs) -ForegroundColor Magenta
  # Write-Host ('$Recursive: {0}' -f $Recursive) -ForegroundColor Magenta

  # Hole alle Dateien und filtere die Dirs
  $Items = Get-ChildItem -File -Force -Path $SrcPath -Filter $Pattern -Recurse:$Recursive | ? {

    # Die $ExcludeDirs beziehen sich nicht auf $SrcPath,
    # sondern nur auf SubDirs
    # Delta zum SrcPath berechnen. Hat ein führendes /
    $DirDelta_zumSrcRoot = $_.FullName.Substring($SrcPath.Length)
    # Write-Host ('$DirDelta_zumSrcRoot: {0}' -f $DirDelta_zumSrcRoot) -ForegroundColor Yellow

    ForEach ($ExcludeDir in $ExcludeDirs) {
      #  $_.FullName -notmatch '\\.git($|\\)' -and $_.FullName -notmatch '\\Documents($|\\)'
      $RgxExcludeDir = ( '/{0}($|/)' -f [RegEx]::Escape($ExcludeDir) )
      # Write-Host ("`nTeste: {0} » {1}`n" -f $DirDelta_zumSrcRoot, $RgxExcludeDir) -ForegroundColor Cyan

      # If ($_.FullName -match $RgxExcludeDir) {
      If ($DirDelta_zumSrcRoot -match $RgxExcludeDir) {
        # Write-Host ('Überspringe: {0}' -f $_.FullName) -ForegroundColor Green
        Return $False
      }
    }
    Return $True
  }
    
  ForEach ($Item in $Items) {
    $RelPath = $Item.FullName.Substring($SrcPath.Length)
    $TargetFile = Join-Path -Path $DstPath -ChildPath $RelPath
    If ($Item.PSIsContainer) {
      $TargetDir = $TargetFile
    } Else {
      $TargetDir = Split-Path $TargetFile
    }
    # Write-Host ('  $Item.FullName: {0}' -f $Item.FullName) -ForegroundColor Cyan
    # Write-Host ('  $Item.Directory: {0}' -f $Item.Directory) -ForegroundColor Cyan
    # Write-Host ('  $RelPath: {0}' -f $RelPath) -ForegroundColor Yellow
    # Write-Host ('  $TargetFile: {0}' -f $TargetFile) -ForegroundColor Yellow
    # Write-Host ('  $TargetDir: {0}' -f $TargetDir) -ForegroundColor Yellow

    If (!(Test-Path -LiteralPath $TargetDir)) {
      $Null = New-Item -Path $TargetDir -ItemType Directory -Force
    }
    # Write-Host ("`nKopiere: {0} » {1}" -f $Item.FullName, $TargetFile) -ForegroundColor yellow
    Copy-Item -Path $Item.FullName -Destination $TargetFile -Force
  }
}


# Löst einen Pfad auf und löst keine Exception aus, wenn er nicht exisitert
Function Resolve-Path-Fixed($RelPath) {
	Try {
    $oResolvedPath = Resolve-Path $RelPath -ErrorAction Stop
    Return $oResolvedPath.Path
  } Catch {
    # Resolve-Path: Cannot find path '/home/ad/backup' because it does not exist.
    $ErrorMsg = ($_.Exception.Message).Trim()
    $firstPos = $ErrorMsg.IndexOf("'")
    $lastPos = $ErrorMsg.LastIndexOf("'")
    if ($firstPos -ne -1 -and $lastPos -ne -1 -and $lastPos -gt $firstPos) {
      $ResolvedPath = $ErrorMsg.Substring($firstPos + 1, $lastPos - $firstPos - 1)
      Return $ResolvedPath
    } else {
      Throw
    }
  }
}


# 
Function Backup-ServiceConfigs {
  Param (
    [Parameter(Position=0, Mandatory)]
    [String]$BackupDir,
    [Parameter(Position=1, Mandatory)]
    [Array]$SrvcConfigBackupInfo,
    [Int]$Ident = 0,
    [Int]$Col1Width = 20
  )

  # Backup-Dir erzeugen
  $Null = New-Item -Path $BackupDir -ItemType Directory -Force -ErrorAction SilentlyContinue  
    
  # Für jeden Dienst die Files kopieren
  Foreach ($SrvcBackupInfo in $SrvcConfigBackupInfo) {
    Write-Host-2Col -Ident $Ident -Col1Width $Col1Width -TxtCol1 'Backup' -TxtCol2 $SrvcBackupInfo.Name -Col2FGColor Cyan

    # Haben wir einen Backupjob definiert?
    $BackupJob = $SrvcBackupInfo.BackupJob
    If ($BackupJob) {
      Switch ($BackupJob.Name) {
        'Backup-Crontab' {
          # Zieldatei berechnen
          # Bestimme das Zielverzeichnis
          $CrontabBackupFile = Join-Path -Path $BackupDir -ChildPath ('contab export for {0}' -f $BackupJob.Param)
          Backup-Crontab $BackupJob.Param $CrontabBackupFile
        }
      }
    }


    # Die Files kopieren
    $CopyProfiles = $SrvcBackupInfo.CopyProfiles
    ForEach ($Profile in $CopyProfiles) {
      # Extrahiere den Pfad und das Muster
      $Recursive = $Profile.Recursive
      $SrcPath = (Split-Path -Path $Profile.Src -Parent)
      $Ptrn = (Split-Path -Path $Profile.Src -Leaf)
      # Write-Host ('$Ptrn: {0}' -f $Ptrn) -ForegroundColor Magenta

      $ExcludeSubDirs = $Profile.ExcludeSubDirs
      $PrintWarning = $Profile.PrintWarning

      If (!([String]::IsNullOrWhiteSpace($PrintWarning))) {
        Write-Host-2Col -Ident $Ident -Col1Width $Col1Width -TxtCol1 '' -TxtCol2 $PrintWarning -Col2FGColor Red
      }

      # Bestimme das Zielverzeichnis
      $ThisBackupDir = Join-Path -Path $BackupDir -ChildPath $SrcPath.TrimStart('/')

      # Erstelle das Zielverzeichnis, falls es nicht existiert
      If (!(Test-Path -Path $ThisBackupDir)) {
        Write-Host-2Col -Ident (1+$Ident) -Col1Width $Col1Width -TxtCol1 'Dir' -TxtCol2 $ThisBackupDir
        $Null = New-Item -Path $ThisBackupDir -ItemType Directory -Force
      }

      # Kopiere die Dateien
      Copy-Item-ExcludeDirs -SrcPath $SrcPath -DstPath $ThisBackupDir -Pattern $Ptrn -ExcludeDirs $ExcludeSubDirs -Recursive:$Recursive
    }
  }
}




## Prepare

Write-Host "`nKopiere Backup aller Server-Config Files" -ForegroundColor Yellow


If ([String]::IsNullOrWhiteSpace($BackupDir)) {
  Write-Host 'Parameter -BackupDir nicht definiert' -ForegroundColor Magenta
  Write-Host "  ❯ Nütze Default Backup Dir: $DefaultBackupDir`n" -ForegroundColor Magenta
  $BackupDir = $DefaultBackupDir
}


Write-Host "BackupDir: $BackupDir"

# Das effektive Ziel von $BackupDir auflösen
$BackupDir = Resolve-Path-Fixed $BackupDir
# Info ausgeben
Write-Host-2Col -Ident 0 -Col1Width $Col1Width -TxtCol1 'Backup-Dir' -TxtCol2 $BackupDir -Col2FGColor Green



## Main

Backup-ServiceConfigs -BackupDir $BackupDir -SrvcConfigBackupInfo $SrvcConfigBackupInfo -Col1Width $Col1Width -Ident 1


