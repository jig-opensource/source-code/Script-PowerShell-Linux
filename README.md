# Script PowerShell Linux

💡 Git-Repository:
* GitLab-URL Project: https://gitlab.com/jig-opensource/source-code/Scripts-PowerShell-Linux
* GitLab-URL File: https://gitlab.com/jig-opensource/source-code/Scripts-PowerShell-Linux/-/blob/main/README.md
* `git clone https://gitlab.com/jig-opensource/source-code/Scripts-PowerShell-Linux.git`


# Aktuelle lokale git clones

- bapx006-1:~/# <br>
  `/usr/local/src/gitlab/ScriptPowerShellLinux/repo/`


# Bietet PowerShell-Script für Linux

- Scripts werden hierhin installiert: <br>
  `/usr/local/bin/`

 

# Einrichten
1. Herunterladen von `get-Linux-PowerShell-Scripts.ps1`: 
   ```
   cd `/usr/local/bin`
   wget https://gitlab.com/jig-opensource/source-code/Script-PowerShell-Linux/-/raw/main/get-Linux-PowerShell-Scripts.ps1?ref_type=heads -O get-Linux-PowerShell-Scripts.ps1
   ```

2. Ausführen und so die aktuellen PS-Scripts herunterladen:

   ```
   # Das Script ausführbar machen
   chmod +x get-Linux-PowerShell-Scripts.ps1
   # Das Script starten und alle Scripts herunterladen / aktualisieren
   ./get-Linux-PowerShell-Scripts.ps1 -ZipURL "https://gitlab.com/jig-opensource/source-code/Script-PowerShell-Linux/-/archive/main/Script-PowerShell-Linux-main.zip" -ZipPathInZip . -TargetPath "/usr/local/bin"
   ```



# Entwicklungsumgebung

- Der Code wird i.d.R. in Linux hierhin geklont: <br>
  `/usr/local/src/gitlab/ScriptPowerShellLinux/repo`

- Arbeiten mit dem Git-Repository: ssh oder https?

  ❯ Die https URL nützen, damit man in Linux ad-hoc mit dem git repo username / PW arbeiten kann <br>
  (und für die kurze Arbeit keinen Git Repo SSH Key einrichten muss)

- Klonen

  ```
  # Klonen ins aktuelle Verzeichnis
  cd /usr/local/src/gitlab/nginx-ldap/repo
  git clone https://gitlab.com/jig-opensource/source-code/nginx-ldap.git .
  ```

- Files zu Gitlab übertragen
  - Status prüfen <br>
    `git status`

  - Lokale Änderungen übernehmen <br>
    `git add .` <br>
    `git status`

  - Local commit <br>
    `git commit -m "Kommentare"`

  - Lokale Änderungen zum Server übertragen <br>
    `git push`



