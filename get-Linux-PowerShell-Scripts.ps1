#!/usr/bin/pwsh

# 001, 240521, Tom@jig.ch

# Lädt eine git repo zip Datei herunter
# und entpackt daraus ein SubDir
# in ein lokales Verzeichnis, meist /usr/local/bin

# !Ex
# ./get-Linux-PowerShell-Scripts.ps1 -ZipURL "https://gitlab.com/jig-opensource/source-code/Script-PowerShell-Linux/-/archive/main/Script-PowerShell-Linux-main.zip" -ZipPathInZip . -TargetPath "/usr/local/bin"


# Einrichten
# 1. Herunterladen: ./get-Linux-PowerShell-Scripts.ps1
#   cd `/usr/local/bin`
#   wget https://gitlab.com/jig-opensource/source-code/Script-PowerShell-Linux/-/raw/main/get-Linux-PowerShell-Scripts.ps1?ref_type=heads -O get-Linux-PowerShell-Scripts.ps1
#
# 2. Ausführen und so die aktuellen PS-Scripts herunterladen
#   chmod +x get-Linux-PowerShell-Scripts.ps1
#   ./get-Linux-PowerShell-Scripts.ps1 -ZipURL "https://gitlab.com/jig-opensource/source-code/Script-PowerShell-Linux/-/archive/main/Script-PowerShell-Linux-main.zip" -ZipPathInZip . -TargetPath "/usr/local/bin"



Param(
  [Parameter(Mandatory)]
  # Die git zip URL
  [String]$ZipURL,
    
  [Parameter(Mandatory)]
  # Der relative Pfad im Zip, aus dem die File gelesen und zu $TargetPath kopiert werden
  [String]$ZipPathInZip,
    
  [Parameter(Mandatory)]
  # Der Zielpfad für die Files. i.d.R.: /usr/local/bin
  [String]$TargetPath,

  # Soll der Pfad im Zip rekursiv nach den Files zum Kopieren suchen?
  [Switch]$Recurse,
  # Gibt Log-Infos aus
  [Switch]$Log=$True
)


# Temporäre Verzeichnisse für das ZIP-Archiv und die extrahierten Dateien festlegen
$TempZipDir = "/tmp/git-temp-dir"
$TempZipFile = "$TempZipDir/tempfile.zip"
$TempZipExtractDir = "$TempZipDir/extract"

# Dirs erzeugen
New-Item -ItemType Directory -Path $TargetPath -Force | Out-Null
New-Item -ItemType Directory -Path $TempZipExtractDir -Force | Out-Null

# ZIP-Archiv herunterladen
Invoke-WebRequest -Uri $ZipURL -OutFile $TempZipFile

# ZIP-Archiv in temporäres Verzeichnis entpacken
Expand-Archive -Path $TempZipFile -DestinationPath $TempZipExtractDir -Force

# Das erste Verzeichnis im Zip erkennen
$MainZipSubDir = Get-ChildItem $TempZipExtractDir | select -First 1 -ExpandProperty Name
# Write-Host $MainZipDir

# Das Haupt-Zip-Dir dem relativen Quellpfad zufügen
$StartCopyZipSubDir = Join-Path $MainZipSubDir $ZipPathInZip
# Write-Host $StartCopyZipSubDir

# Das absolute Quell-Dir
$StartCopyZipDir = Join-Path $TempZipExtractDir $StartCopyZipSubDir
# Den eff. Pfad berechnen
$StartCopyZipDir = Resolve-Path $StartCopyZipDir
# Write-Host ("`nStartCopyZipDir: {0}" -f $StartCopyZipDir)

# Dateien ins Zielverzeichnis kopieren
# $SourcePath = Join-Path -Path $TempZipExtractDir -ChildPath $ZipPathInZip
$Files = Get-ChildItem -Path $StartCopyZipDir -Recurse:$Recurse
$OverwriteAllFiles = $False
ForEach ($File in $Files) {
  # Write-Host ("`nFile.Name      : {0}" -f $File.Name)
  # Write-Host ('File.FullName  : {0}' -f $File.FullName)
  $relativePath = $File.FullName.Replace($StartCopyZipDir, '').TrimStart('/')
  # If ($Log) { Write-Host ('Kopiere: {0}' -f $relativePath) }
  $targetFile = Join-Path -Path $TargetPath -ChildPath $relativePath

  $targetFileExists = Test-Path -Path $targetFile

  $CopyFile = $True
  If ($OverwriteAllFiles -eq $False -And $targetFileExists) {
    Write-Host ('Existiert: {0}' -f $targetFile) -ForegroundColor Red
    $response = Read-Host 'Überschreiben? (Ja/Nein/Alle)'
    $OK = $False
    While ($OK -eq $False) {
      Switch ($response) {
        { @('j', 'y', 'ja') -contains $_ } { $OK = $True; Break }
        { @('n', 'nein') -contains $_ } { Write-Host ' > skipped'; $OK = $True; $CopyFile = $False; Continue }
        { @('a', 'alle') -contains $_ } { $OverwriteAllFiles = $True; $OK = $True; Break }
        Default { $response = Read-Host 'Überschreiben? (Ja/Nein/Alle)'; Break }
      }
    }
  }

  If ($CopyFile) {
    If ($Log) { 
      If ($targetFileExists) {
        Write-Host ('Überschreibe: {0}' -f $targetFile) -ForegroundColor Magenta
      } Else {
        Write-Host ('Erzeuge     : {0}' -f $targetFile)
      }
    }
    Copy-Item -Path $file.FullName -Destination $targetFile -Force
  }
}

# Temporäre Daten löschen
Remove-Item -Path $TempZipDir -Recurse -Force
