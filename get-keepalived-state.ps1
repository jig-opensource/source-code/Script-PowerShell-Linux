#!/usr/bin/pwsh

# Analysiert den Status von Keepalived
# und zeigt das Resultat an


# 001, 240529, tom@jig.ch



## Config
$KeepalivedCfg = '/etc/keepalived/keepalived.conf'
$Col1 = 16



## Functions


# Liefert die IP-Adressen des Hosts
Function Get-Linux-Host-IPAddrs() {
  # Die IP-Adressen des Hosts abrufen
  $ThisHostIPS = $(ip -o -4 addr show scope global | awk '{print $4}' | cut -d '/' -f1)
  $ThisHostIPS 
}


# Abrufen, welche IP-Adresse Keepalived als Shared Adresse nützt
Function Get-Keepalived-Virtual-IPAddrs($KeepalivedCfg) {
  $VirtualIPAddr = Get-Content $KeepalivedCfg -Raw | `
                   Select-String -Pattern '.*\bvirtual_ipaddress\b\s*{\s*(.*)\s*}' | % {
                     $_.Matches.Groups[1].Value.Trim()
                   }
  $VirtualIPAddr
}


# Liest die Keepalived Service …State Properties
Function Get-Keepalived-Service-States() {
  # Die Service Status Props abrufen

  # Varianten:
  # systemctl show -p ActiveState --value keepalived
  # systemctl show -p SubState --value keepalived
  # systemctl is-active --quiet keepalived && echo Service is running
  # systemctl show keepalived | grep --color=never State=

  # Infos zum keepalived service abrufen
  $Res = systemctl show keepalived
  # Hashtable erzeugen
  $ResStates = $Res | ConvertFrom-StringData
  $htStates = @{}
  $ResStates | % {
    # $ItemEnum = $_.GetEnumerator()
    # Alle *State Properties in die HT übertragen
    $Item = $_.GetEnumerator() | Select Name, Value
    If ($Item.Name  -like "*State") {
      $htStates.Add( $Item.Name, $Item.Value )
    }
  }

  # Liefert diese Props:
  # $htStates.ActiveState   active
  # $htStates.SubState      running
  # $htStates.LoadState     loaded
  # $htStates.UnitFileState enabled
  # $htStates.FreezerState  running

  $htStates
}



## Prepare

# Daten bestimmen
$VirtualIPAddr = Get-Keepalived-Virtual-IPAddrs $KeepalivedCfg
$ThisHostIPS = Get-Linux-Host-IPAddrs

# Hat der Host die Keepalived Shared IP-Adresse?
$HostIsActiveNode = ($ThisHostIPS | ? { $_.Trim() -eq $VirtualIPAddr.Trim() }).Count -ge 1



## Main

Write-Host "`n`nKeepalived Service-Infos" -ForegroundColor Yellow


# Ist die Keepalived-Node aktiv oder passiv?
Write-Host ("{0,-$Col1}: " -f 'Keepalived Node') -NoNewLine
If ( $HostIsActiveNode ) {
  Write-Host 'Aktiv' -ForegroundColor Magenta
} Else {
  Write-Host 'Passiv' -ForegroundColor Magenta
}

# Die Virtuelle IP-Adresse von Keepalived
Write-Host ("{0,-$Col1}: " -f 'VirtualIPAddr') -NoNewLine
Write-Host $VirtualIPAddr -ForegroundColor Magenta


# Die Service Status Props abrufen
Write-Host "`nService Status:" -ForegroundColor Yellow

$KeepalivedSrvcStates = Get-Keepalived-Service-States

# Die State-Props anzeigen
# Write-Host ($htStates | FT -HideTableHeaders | Out-String)

ForEach ($Pair in $KeepalivedSrvcStates.GetEnumerator()) {
  Write-Host ("{0,-$Col1}: " -f $Pair.Name) -NoNewLine
  Write-Host $Pair.Value -ForegroundColor Green
}


