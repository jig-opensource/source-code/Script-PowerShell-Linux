#!/usr/bin/pwsh

# Zeigt Infos zum System an:
# - Genüzte Dienste
# - Scripts

# Diese Variante ist für nginx-Hosts

# 001, 240529, tom@jig.ch
# 002, 240604
#   Infos erweitert


## Allenfalls als root starten
If ( (id -u) -ne 0) {
  Write-Host 'Das Skript benötigt Root-Rechte. Starte es als root' -ForegroundColor Red
  # Konstruktion des Befehls für die Neuausführung mit sudo
  $command = ('sudo pwsh -File {0}' -f $MyInvocation.MyCommand.Path)
  # Ausführung des Befehls
  Invoke-Expression $command
  # Beenden des aktuellen Skripts, um Doppelungen zu vermeiden
  Exit
}


## Config
$KeepalivedCfg = '/etc/keepalived/keepalived.conf'
$Col1 = 24


# Spezielle Files
$SpecialFiles = @(
  @{
    Comment = 'Files von nginx-ldap-auth'
    Files = @(
      '/etc/systemd/system/nginx-ldap-auth.service'
      '/etc/nginx/nginx-ldap-auth-daemon'
      '/etc/nginx/nginx-ldap.conf'
    )
  }, @{
    Comment = 'Script zum Starten der unison File- / Konfig-Syncronisation'
    Files = @(
      '/root/.unison/start-unison.ps1'
    )
  }
)


# Wichtige Dirs
$SpecialDirs = @(
  @{
    Dir = '/usr/local/src/gitlab'
    Comment = 'Lokaler Git Source Klon'
  }, @{
    Dir = '/usr/local/bin'
    Comment = 'Lokale Scripts'
  }, @{
    Dir = '/root/.unison'
    Comment = 'Die unison profile und scripts für die File- / Config-Sync'
  }, @{
    Dir = '/etc/keepalived'
    Comment = 'Die keepalived-Konfig'
  }, @{
    Dir = '/etc/nginx'
    Comment = 'Die nginx-Konfig'
  }, @{
    Dir = '/var/www/html'
    Comment = 'Die statischen Files für nginx'
  }
)




## Functions


# Konvertiert einen String wie 'yellow' in [ConsoleColor]
# Bei einem Fehler wird White zurückgegeben
Function ConvertTo-ConsoleColor {
    Param (
        [String]$colorString
    )

    try {
        # Try to convert the string to a ConsoleColor
        $color = [ConsoleColor]::Parse([ConsoleColor], $colorString)
    } catch {
        Write-Error ("Unbekannte ConsoleColor: '{0}'" -f $colorString) -ForegroundColor Red
        # If there is an error, default to ConsoleColor.White
        $color = [ConsoleColor]::White
    }

    return $color
}



# Liefert die IP-Adressen des Hosts
Function Get-Linux-Host-IPAddrs() {
  # Die IP-Adressen des Hosts abrufen
  $ThisHostIPS = $(ip -o -4 addr show scope global | awk '{print $4}' | cut -d '/' -f1)
  $ThisHostIPS 
}


# Abrufen, welche IP-Adresse Keepalived als Shared Adresse nützt
Function Get-Keepalived-Virtual-IPAddrs($KeepalivedCfg) {
  $VirtualIPAddr = Get-Content $KeepalivedCfg -Raw | `
                   Select-String -Pattern '.*\bvirtual_ipaddress\b\s*{\s*(.*)\s*}' | % {
                     $_.Matches.Groups[1].Value.Trim()
                   }
  $VirtualIPAddr
}


# Liest die Keepalived Service …State Properties
Function Get-Keepalived-Service-States() {
  # Die Service Status Props abrufen

  # Varianten:
  # systemctl show -p ActiveState --value keepalived
  # systemctl show -p SubState --value keepalived
  # systemctl is-active --quiet keepalived && echo Service is running
  # systemctl show keepalived | grep --color=never State=

  # Infos zum keepalived service abrufen
  $Res = systemctl show keepalived
  # Hashtable erzeugen
  $ResStates = $Res | ConvertFrom-StringData
  $htStates = @{}
  $ResStates | % {
    # $ItemEnum = $_.GetEnumerator()
    # Alle *State Properties in die HT übertragen
    $Item = $_.GetEnumerator() | Select Name, Value
    If ($Item.Name  -like "*State") {
      $htStates.Add( $Item.Name, $Item.Value )
    }
  }

  # Liefert diese Props:
  # $htStates.ActiveState   active
  # $htStates.SubState      running
  # $htStates.LoadState     loaded
  # $htStates.UnitFileState enabled
  # $htStates.FreezerState  running

  $htStates
}


# Gibt Text in zwei Spalten aus
Function Write-Host-2Col() {
  Param (
    [Parameter()]
    [Int]$Ident=0,
    [Parameter(Mandatory, Position=0)]
    [Int]$Col1Width,
    [Parameter(Mandatory, Position=1)]
    [String]$TxtCol1,
    [Parameter(Position=2)]
    [String]$TxtCol2,
    [Parameter(Position=3)]
    [String]$Col2FGColor = 'White'
  )

  $oFGColor = ConvertTo-ConsoleColor $Col2FGColor

  Write-Host ("{0}{1,-$Col1Width}: " -f ('  ' * $Ident), $TxtCol1) -NoNewLine
  Write-Host ("{0}" -f $TxtCol2) -ForegroundColor $oFGColor
 
}


# Gibt die Infos von $SpecialFiles aus
Function Print-SpecialFiles() {
  Write-Host "`nSpezielle Files:" -ForegroundColor Yellow
  $SpecialFiles | % {
    Write-Host ('  {0}' -f $_.Comment) -ForegroundColor Cyan
    $_.Files | % {
      Write-Host ('    {0}' -f $_)
    }
  }
}


# Gibt die Infos von $SpecialDirs aus
Function Print-SpecialDirs() {
  Write-Host "`nSpezielle Dirs:" -ForegroundColor Yellow
  $SpecialDirs | % {
    Write-Host ('  {0}' -f $_.Dir) -ForegroundColor Cyan
    Write-Host ('    {0}' -f $_.Comment)
  }
}


# Liefert von einem Service nur den Status-Text zurück, also z.B. nur 'active'
Function Get-Service-State() {
  Param (
    [Parameter(Mandatory, Position=1)]
    [String]$ServiceName
  )

  $Status = (systemctl show -p ActiveState --value $ServiceName).Trim()
  Return $Status
}


# Liefert alle Crontab-Jobs, also ohne Kommentare
# !TT Crontab bearbeiten: crontab -e
Function Get-Crontab-Jobs() {
  $CrontabContent = crontab -l 2>&1
  # Entfernen von leeren Zeilen und Kommentaren
  $CrontabJobs = $CrontabContent | ? { $_ -notmatch '^\s*#' -and $_ -notmatch '^\s*$' }
  Return $CrontabJobs
}


# Liefert die Anzahl Crontab Jobs
Function Get-Anz-Crontab-Jobs() {
  $CrontabJobs = Get-Crontab-Jobs
  If ($CrontabJobs -eq $Null) {
    Return 0
  }
  If ($CrontabJobs -is [Array]) {
    Return $CrontabJobs.Count
  }
  If ( [String]::IsNullOrWhiteSpace($CrontabJobs) ) {
    Return 0
  }

  Return 1
}


# $True, wenn mind. 1 Crontab existiert
Function Has-Crontab-Jobs() {
  $AnzCrontabJobs = Get-Anz-Crontab-Jobs
  If ($AnzCrontabJobs -eq 0) {
    Return $False
  }
  Return $True
}


# Gibt Infos zu Crontab aus
# !TT: Bearbeiten: crontab -e 
Function Print-Crontab-Info() {
  Param (
    [Parameter()]
    [Int]$Ident=0,
    [Parameter(Mandatory, Position=0)]
    [Int]$Col1Width
  )

  $HasCrontabJobs = Has-Crontab-Jobs
  $AnzCrontabJobs = Get-Anz-Crontab-Jobs
 
  Write-Host "`nCrontab-Infos:" -ForegroundColor Yellow
  Write-Host-2Col -Ident $Ident -Col1Width $Col1 -TxtCol1 'Anzahl Crontab Jobs' -TxtCol2 $AnzCrontabJobs -Col2FGColor Cyan
  If ($AnzCrontabJobs -gt 0) {
    $CrontabJobs = Get-Crontab-Jobs
    $CrontabJobs | % {
      Write-Host-2Col -Ident (1+$Ident) -Col1Width $Col1 -TxtCol1 'Job' -TxtCol2 $_
    }
  }
}



# Zeigt den Status eines Dienstes an
Function Print-Service-State() {
  Param (
    [Parameter()]
    [Int]$Ident=0,
    [Parameter(Mandatory, Position=0)]
    [Int]$Col1Width,
    [Parameter(Mandatory, Position=1)]
    [String]$ServiceName
  )

  $Status = Get-Service-State $ServiceName
  If ($Status -eq 'active') {
    Write-Host-2Col -Ident $Ident -Col1Width $Col1 -TxtCol1 $ServiceName -TxtCol2 $Status -Col2FGColor Green
  } Else {
    Write-Host-2Col -Ident $Ident -Col1Width $Col1 -TxtCol1 $ServiceName -TxtCol2 $Status -Col2FGColor Magenta
  }
}



# Gibt basis Sysinfos aus
Function Print-Basic-SysInfo() {
	# Daten bestimmen
  $OSNameUndVersion = (Get-Content /etc/os-release | Select-String -Pattern 'PRETTY_NAME').Line.Split('=')[1].Trim('"')
  $HostName = [System.Net.Dns]::GetHostName()

  $VirtualIPAddr = Get-Keepalived-Virtual-IPAddrs $KeepalivedCfg
  $ThisHostIPS = Get-Linux-Host-IPAddrs

  # Hat der Host die Keepalived Shared IP-Adresse?
  $HostIsActiveNode = ($ThisHostIPS | ? { $_.Trim() -eq $VirtualIPAddr.Trim() }).Count -ge 1

  # Infos ausgeben
  Write-Host "`nHost-Infos:" -ForegroundColor Yellow
  Write-Host-2Col -Ident 1 -Col1Width $Col1 -TxtCol1 'OS' -TxtCol2 $OSNameUndVersion -Col2FGColor Green
  Write-Host-2Col -Ident 1 -Col1Width $Col1 -TxtCol1 'Hostname' -TxtCol2 $HostName -Col2FGColor Green
  Write-Host-2Col -Ident 1 -Col1Width $Col1 -TxtCol1 'IP-Adressen' -TxtCol2 ($ThisHostIPS -join ', ') -Col2FGColor Green
  Write-Host-2Col -Ident 1 -Col1Width $Col1 -TxtCol1 'Keepalived virtual IP' -TxtCol2 $VirtualIPAddr -Col2FGColor Green

  If ( $HostIsActiveNode ) {
    Write-Host-2Col -Ident 1 -Col1Width $Col1 -TxtCol1 'Keepalived Node' -TxtCol2 'Master' -Col2FGColor Cyan
  } else {
    Write-Host-2Col -Ident 1 -Col1Width $Col1 -TxtCol1 'Keepalived Node' -TxtCol2 'Slave' -Col2FGColor Magenta
  }

  # Service-Infos ausgeben
  Write-Host "`nDienste:" -ForegroundColor Yellow
  Print-Service-State -Ident 1 $Col1 'keepalived'
  Print-Service-State -Ident 1 $Col1 'nginx-ldap-auth.service'
  Print-Service-State -Ident 1 $Col1 'nginx'

}



## Main

Print-Basic-SysInfo

Print-SpecialDirs
Print-SpecialFiles

Print-Crontab-Info -Ident 1 -Col1Width $Col1

# Scripts
Write-Host "`nScripts:" -ForegroundColor Yellow
Get-ChildItem -LiteralPath /usr/local/bin

