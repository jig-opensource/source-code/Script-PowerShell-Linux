#!/usr/bin/pwsh

# Richtet die private/publich SSH Keys ein,
# um damit eine ssh-Verbindung zu einem Ziel-Server ohne PW aufzubauen


# 001, 240521, tom@jig.ch
# 001, 240522
#   Neu: -MyDebug



# Wichtig:
# Der Client, von dem aus der SSH-Zugriff erfolgt:
#  - hat den private key in ~/.ssh
#
# Der Server, auf den der SSH-Zugriff erfolgt:
#  - Speichert den pulic key in /home/<user>/.ssh/authorized_keys


# Erzeugt:
#       ~/.ssh/
#       id_root@bapx006-2.key
#       id_root@bapx006-2.pub
#
# Überträgt den Publig Key zum Server, auf den zugegriffen werden soll

# !Ex
# /usr/local/bin/set-ssh-remote-access.ps1 -TargetHost bapx006-2


[CmdletBinding()]
param(
  [Parameter(Position=0,Mandatory)]
  # Ziel-Rechner, zu dem die SSH-Verbindung eingerichtet werden soll
  # » Kriegt den Public Key
  [String]$TargetHost,
  [Switch]$Force,
  [Switch]$MyDebug
)



## Config

$SshConfigFullFileName = "~/.ssh/config"

# SSH Login zum Zielrechner, e.g. ad@zielrechner
$ZielRechnerLogin = "$(whoami)@${TargetHost}"

# Namen der Public- & Private-Keys
# <loginname>@<hostname>
$SshKeyComment = "$(whoami)@$(hostname)-$(date +'%y%m%d')"
$SshFileName = "id_$(whoami)@$(hostname)"
$SshFileNameKey = "${SshFileName}.key"
$SshFileNamePub = "${SshFileName}.pub"

$SshFullFileName = "~/.ssh/$SshFileName"
$SshFullFileNameKey = "~/.ssh/$SshFileNameKey"
$SshFullFileNamePub = "~/.ssh/$SshFileNamePub"

# Debug
# Write-Host $SshKeyComment
# Zeigen beide das gleiche:
#   Write-Host ~/.ssh/$SshFileNamePub
#   Write-Host ~/.ssh/"$SshFileNamePub"

# Write-Host $SshFullFileName
# Write-Host $SshFullFileNameKey
# Write-Host $SshFullFileNamePub


# Wenn $Debug aktiv ist, wird die Info zur Variable angezeigt
Function Log-Variable($VarName, $VarValue) {
	If ($MyDebug) {
		Write-Host ('Log: {0}: {1}' -f $VarName, $VarValue) -ForegroundColor Cyan
  }
}


# Löst den Hostnamen auf:
# $TargetHostName, $ThisFQDN = Get-Hostnames bapx006-2
# und liefert:
# bapx006-2
# bapx006-2.akros.ch
Function Get-Hostnames($HostName) {
  Try {
    $FQDN = [System.Net.Dns]::GetHostByName($HostName) | select -ExpandProperty HostName
    $TargetHostName = $FQDN.Split('.') | select -First 1
    Return @($TargetHostName, $FQDN)
  } Catch {
    Write-Error "Host nicht gefunden: '$HostName'"
    Exit
  }
}


# Zählt in einer Textdatei, wie oft $TextPattern vorkommt
Function Count-Words($FileName, $TextPattern) {
  $Anz = (Get-Content $FileName | Select-String -Pattern $TextPattern -AllMatches).Matches.Count
  Return $Anz
}



## Prepare

# Allenfalls Log Infos ausgeben
Log-Variable '$SshConfigFullFileName' $SshConfigFullFileName
Log-Variable '$ZielRechnerLogin' $ZielRechnerLogin
Log-Variable '$SshKeyComment' $SshKeyComment
Log-Variable '$SshFileName' $SshFileName
Log-Variable '$SshFileNameKey' $SshFileNameKey
Log-Variable '$SshFileNamePub' $SshFileNamePub
Log-Variable '$SshFullFileName' $SshFullFileName
Log-Variable '$SshFullFileNameKey' $SshFullFileNameKey
Log-Variable '$SshFullFileNamePub' $SshFullFileNamePub



# Das aktuelle Dir merken
pushd .

# Ins home-Dir des Users wechseln
# root          » /root
# ad            » /home/ad
cd ~


# Vom Ziel den Hostnamen und den FQDN bestimmen
$TargetHostName, $TargetHostFQDN = Get-Hostnames $TargetHost
Log-Variable '$TargetHostFQDN' $TargetHostFQDN
Log-Variable '$TargetHostName' $TargetHostName


# Haben wir die Keys schon?
$CreateKeys = $True
If ((Test-Path -PathType Leaf -LiteralPath $SshFullFileNameKey) `
   -AND (Test-Path -PathType Leaf -LiteralPath $SshFullFileNamePub)) {
  # Debug
  If ($MyDebug) { Write-Host ('Log: Private Key Existiert: {0}' -f $SshFullFileNameKey) -ForegroundColor Cyan }
  If ($MyDebug) { Write-Host ('Log: Public Key Existiert: {0}' -f $SshFullFileNamePub) -ForegroundColor Cyan }

  If ($Force) {
    # Debug
    If ($MyDebug) { Write-Host ('Log: -Force aktiv') -ForegroundColor Cyan }

    Write-Host 'Keys existieren schon:'
    Write-Host '  » Keys werden neu erzeugt'
    $Redeploy = $False;
  } Else {
    Write-Host 'Keys existieren schon:'
    Write-Host '  » Public-Key wird neu zum Ziel-Server übertragen'
    Write-Host "`nInfo:" -ForegroundColor Yellow
    Write-Host '  » Mit -force werden die Keys neu erzeugt' -ForegroundColor Yellow
    $Redeploy = $True; $CreateKeys = $False
  }
} Else {
  If ($MyDebug) { Write-Host ('Log: Private und Public Keys existieren nicht') -ForegroundColor Cyan }
}


# Überträgt und registriert den Public Key
Function TransferAndRegister-Public-Key($SshFullFileNameKey, $SshFullFileNamePub, $ZielRechnerLogin) {

  # Den Public Key lesen und den Kommentar entfernen,
  # um auf dem Zielrechner zu prüfen, ob der Key schon erfasst wurde
  $ThisPubKeyWithComment = Get-Content $SshFullFileNamePub -Raw
  $ThisPubKey = ($ThisPubKeyWithComment -split ' ' | select -First 2) -join ' '

  # Info zum manuellen Übertragen des Public Keys anzeigen
  Write-Host "`n`nInfo: Optional kann der Public Key manuell auf den Ziel-Rechner übertragen werden:" -ForegroundColor Magenta
  Write-Host '- Login auf dem Zielrechner mit dem User, der sich via SSH Key anmelden soll'
  Write-Host '- cd ~/.ssh'
  Write-Host '- Datei ergänzen:'
  Write-Host '  nano authorized_keys'
  Write-Host '- Inhalt:'
  Write-Host ('{0}' -f $ThisPubKeyWithComment)


  # Die SSH-Befele zum Übertragen des Public Keys
  $SshCommand = @"
    # Allenfalls Dir erzeugen: .ssh
    mkdir -p ~/.ssh
    # Aus ~/.ssh/authorized_keys den Public Key löschen, wenn er schon existiert
    if grep -qF "$ThisPubKey" ~/.ssh/authorized_keys; then
      sed -i.bak "/$ThisPubKey/d" ~/.ssh/authorized_keys;
      echo '  > Alter Public Key wurde gelöscht';
    fi

    # Den Public Key zufügen
    echo "$ThisPubKeyWithComment" >> ~/.ssh/authorized_keys;
    echo '  > Neuer Public Key wurde geschrieben';

"@


  Write-Host "`nÜbertrage den Public Key"
  $Res = ssh $ZielRechnerLogin $SshCommand
  Write-Host $Res


  # Wichtige Info:
  # Mit ssh-copy-id den Key übertragen
  #   ssh-copy-id -i ~/.ssh/$SshFileNamePub $(whoami)@bapx006-2
  #   > Bricht ab, wenn es den zugehörigen private key nicht findet
  #   > Der private key darf deshalb nicht .key heissen

  # T&T
  Write-Host "`nSSH-Login kann jetzt so getestet werden:" -ForegroundColor Green
  Write-Host "ssh -i $SshFullFileNameKey $ZielRechnerLogin" -ForegroundColor Green
}


# Ergänzen: ~/.ssh/config
Function Update-ssh-config($SshConfigFullFileName, $ZielRechnerLogin, $TargetHostName, $TargetHostFQDN, $SshFullFileNameKey) {
  Write-Host "`nPrüfe / Ergänze: $SshConfigFullFileName"

  # Die IP-Adresse bestimmen
  $TargetHostIPAddr = [System.Net.Dns]::GetHostAddresses( $TargetHostFQDN ) | select -ExpandProperty IPAddressToString
  Log-Variable '$TargetHostIPAddr' $TargetHostIPAddr


  # Die ssh config:
  #   Host bapx006-2 bapx006-2.akros.ch 10.1.20.32
  $SSHConfigHostIDs = "$TargetHostName $TargetHostFQDN $TargetHostIPAddr"
  Log-Variable '$SSHConfigHostIDs' $SSHConfigHostIDs

  # Hat die Config schon die gleiche Host ID Config?:
  $AnzHostIDs = Count-Words $SshConfigFullFileName $SSHConfigHostIDs
  $AnzIdentityFile = Count-Words $SshConfigFullFileName $SshFullFileNameKey
  Log-Variable '$AnzHostIDs' $AnzHostIDs
  Log-Variable '$AnzIdentityFile' $AnzIdentityFile

  # Hat ssh config den Zielhost bereits erfasst?
  If ($AnzHostIDs -eq 1 -and $AnzIdentityFile -eq 1) {
    Write-Host ' > SSF Config bereits aktuell'
  } Else {

    # Die Config Info erzeugen
    #   Host zielserver zielserver.domain.com 192.168.1.100
    #     User deinBenutzername
    #     IdentityFile ~/.ssh/dein_privater_schlüssel
    $ConfigContent = @"

# Host, Hostname: definieren immer den Ziel-Server!
Host $SSHConfigHostIDs
    IdentityFile $SshFullFileNameKey
"@


    # Die ssh config Datei ergänzen oder erzeugen
    if (Test-Path $SshConfigFullFileName) {
      Add-Content -Path $SshConfigFullFileName -Value $ConfigContent
      Write-Host " > Ergänzt"
    } else {
      New-Item -Path $SshConfigFullFileName -ItemType File -Value $ConfigContent
      Write-Host " > Erzeugt"
    }


    # Sind die Hosts in der SSH Config eindeutig?
    $AnzHostIDs = Count-Words $SshConfigFullFileName $SSHConfigHostIDs
    If ($AnzHostIDs -gt 1) {
      Write-Host ' > ACHTUNG!' -ForegroundColor Red
      Write-Host "   Die SSH Config hat für den Host '$SSHConfigHostIDs' mehrere Einträge!" -ForegroundColor Red
    }

    Write-Host "`n`n`nBitte prüfen, ob die Datei i.O. ist:" -ForegroundColor Magenta
    Write-Host "File:: $SshConfigFullFileName" -ForegroundColor Magenta
    Write-Host "------------------------------------" -ForegroundColor Magenta

    more $SshConfigFullFileName
  }


  # T&T
  Write-Host "`nSSH-Login klappt nun:" -ForegroundColor Green
  Write-Host "ssh $ZielRechnerLogin" -ForegroundColor Green

}



## Main

# Debug
Log-Variable '$CreateKeys' $CreateKeys


If ($CreateKeys) {
  Write-Host 'Erzeuge Public-/Private-Key'
  # ssh Keys erzeugen

  # Knowhow SSH Keys
  # https://docs.hpc.gwdg.de/getting_started/connecting/ssh_faq/why_recommend_only_certain_key_types/index.html
  #
  # …-sk Keys
  #   Sind kompatibel mit fido2
  #
  # ecdsa
  # ecdsa-sk
  #   NICHT empfohlen, weil nur schwer fehlerfrei implementierbar
  #
  # Empfohlen:
  # ed25519
  #   Nicht überall unterstützt
  # ed25519-sk
  #   Kompatibel mit fido2
  #
  # RSA
  #   Nur 4096 empfohlen
  #   » Als fallback
  #   » Überall unterstützt
  #


  # -N: Kein Passwort
  # -C: Kommentar
  ssh-keygen -t ed25519 -N "" -C "$SshKeyComment" -f  $SshFullFileName

  # Private key umbenennen
  # mv ~/.ssh/"$SshFileName" ~/.ssh/"$SshFileNameKey"
  mv $SshFullFileName $SshFullFileNameKey
}


# Ergänzen: ~/.ssh/config
Update-ssh-config $SshConfigFullFileName $ZielRechnerLogin $TargetHostName $TargetHostFQDN $SshFullFileNameKey


# Den Public Key übertragen und registrieren
TransferAndRegister-Public-Key $SshFullFileNameKey $SshFullFileNamePub $ZielRechnerLogin


# Wieder ins Original Dir
popd
